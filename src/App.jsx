import { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";

function App() {

  useEffect(()=>{
    getWeather("Paris",48.85,2.35)
  },[])

  const[temperature, setTemperature] = useState(0)
  const[selectedCity, setSelectedCity] = useState("Paris")

  function getWeather(city,latitude,longitude){

    setSelectedCity(city)

    axios.get("https://api.open-meteo.com/v1/forecast?latitude="+latitude+"&longitude="+longitude+"&current_weather=true&hourly=temperature_2m,relativehumidity_2m,windspeed_10m")

    .then(data=>{
      const temp = (data.data.current_weather.temperature)
      setTemperature(temp)

    })
    .catch(err=>{
      console.log(err)
    })
  }

  return (
    <>
      <header></header>
      <main>
        <h1>My Weather App</h1>
        <p>
          The current weather at {selectedCity} is <span id="temperature"> {temperature}°C</span>{" "}
        </p>
        <div id="buttons">
          <button onClick={()=> {getWeather("Kochi",9.93,76.26)}}>Kochi</button>
          <button onClick={()=> {getWeather("Alappey",9.49,76.33)}}>Alappey</button>
          <button onClick={()=> {getWeather("Trivandrum",8.52,76.93)}}>Trivandrum</button>
        </div>
      </main>
      <footer></footer>
    </>
  );
}

export default App;
